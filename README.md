## Custom Shipping Plugin

This plugin was developed for **The Foggy Forest** in April 2021

---

## Features

There are many plugins out there that offer manipulation of WooCoomerce shipping charges. The unique thing about this plugin is that it can display not just one combined shipping charge but two shiping charges separately in the checkout page.

The shipping is calculated based on conditions defined in the plugin.

---