=== Alpha Distance Shipping ===
Contributors: sofyansitorus
Tags: woocommerce,woocommerce-shipping,local-shipping,private-shipping
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DGSVXLV7R8BTY
Requires at least: 4.8
Tested up to: 5.2.2
Requires PHP: 5.6
Stable tag: 2.0.8
License: GPL-2.0+
License URI: http://www.gnu.org/licenses/gpl-2.0.txt
