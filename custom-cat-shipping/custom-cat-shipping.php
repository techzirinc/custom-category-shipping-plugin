<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://github.com/sofyansitorus
 * @since             2.0.0
 * @package           ALDS
 *
 * @wordpress-plugin
 * Plugin Name:       Custom Cat Shipping
 * Plugin URI:        
 * Description:       WooCommerce shipping rates calculator.
 * Version:           1.0.1
 * Author:            TashFeen
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ccatship
 * Domain Path:       /languages
 *
 * WC requires at least: 3.0.0
 * WC tested up to: 3.7.0
*/
if ( ! defined( 'ABSPATH' ) ) {
	die;
}


if ( ! function_exists( 'get_plugin_data' ) ) {
	require_once ABSPATH . 'wp-admin/includes/plugin.php';
}


if ( !is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
    return;
}



function ccatship_shipping_method(){
  include plugin_dir_path( __FILE__ ) . 'includes/class-ccatship-shipping.php';
 }

 add_action('woocommerce_shipping_init', 'ccatship_shipping_method');


 function add_ccatship_shipping_method($methods)
 {
	$methods[] = 'Cat_Shipping_Method';
	return $methods;
 }

 add_filter('woocommerce_shipping_methods', 'add_ccatship_shipping_method');


add_action( 'woocommerce_cart_calculate_fees','woocommerce_custom_surcharge' );

add_action( 'wp_head', 'woo_style_init' );
function woo_style_init(){
	if(is_cart()||is_checkout()){
		global $woocommerce;
		$items = $woocommerce->cart->get_cart();
		$enabled=get_option('ccatship_enabled');
		$c_name=get_option('ccatship_c_name');
		$category=get_option('ccatship_category');
		$max_limit=get_option('ccatship_max_limit');
		$per_item_price=get_option('ccatship_per_item_price');
		if(empty($enabled)){
		  $enabled=1;
		}
		if(empty($c_name)){
		  $c_name="Extra Shipping Fee";
		}
		if(empty($category)){
		  $category="gear";
		}
		if(empty($max_limit)){
		  $max_limit=150;
		}
		if(empty($per_item_price)){
		  $per_item_price=20;
		}
		$flag=0;

		if(!empty($items)){
		foreach($items as $item => $values) { 
		 $product_id = $values['product_id'];
		  if(!has_term(  $category,  'product_cat', $product_id )){
			$flag=1;
		  } 
		}
	  } 
	  if ($flag==0) {
		  ?>
			<style>
				#shipping_method,.woocommerce-checkout .woocommerce-shipping-totals{
					display:none !important;
				}
			</style>
		  <?php
	  }
	}
}

add_filter( 'woocommerce_package_rates', 'woocommerce_package_rates' );
function woocommerce_package_rates( $rates ) {
    global $woocommerce;
	$items = $woocommerce->cart->get_cart();
    $enabled=get_option('ccatship_enabled');
    $c_name=get_option('ccatship_c_name');
    $category=get_option('ccatship_category');
    $max_limit=get_option('ccatship_max_limit');
    $per_item_price=get_option('ccatship_per_item_price');
    if(empty($enabled)){
      $enabled=1;
    }
    if(empty($c_name)){
      $c_name="Extra Shipping Fee";
    }
    if(empty($category)){
      $category="gear";
    }
    if(empty($max_limit)){
      $max_limit=150;
    }
    if(empty($per_item_price)){
      $per_item_price=20;
    }
    $flag=0;

    if(!empty($items)){
    foreach($items as $item => $values) { 
      $product_id = $values['product_id'];
      if(!has_term(  $category,  'product_cat', $product_id )){
        $flag=1;
      } 
    }
  } 
  if ($flag==0) {
    if (!empty($rates)) {
      foreach($rates as $key => $rate ) {
		  if($rate->method_id=="flat_rate"){
			$rates[$key]->cost=0;
		  }else{
			  unset($rates[$key]);
		  } 
      }
    }
  }
    
  return $rates;
}


function woocommerce_custom_surcharge(){
  global $woocommerce;

  if ( is_admin() && ! defined( 'DOING_AJAX' ) )
    return;
  $enabled=get_option('ccatship_enabled');
  $c_name=get_option('ccatship_c_name');
  $category=get_option('ccatship_category');
  $max_limit=get_option('ccatship_max_limit');
  $per_item_price=get_option('ccatship_per_item_price');
  if(empty($enabled)){
    $enabled=1;
  }
  	
  if(empty($c_name)){
    $c_name="Hardware Shipping";
  }
  if(empty($category)){
    $category="gear";
  }
  if(empty($max_limit)){
    $max_limit=150;
  }
  if(empty($per_item_price)){
    $per_item_price=20;
  }

  $surcharge = 0.00;
  $items = $woocommerce->cart->get_cart();
  $ccarray=[];
  if(!empty($items)){
    foreach($items as $item => $values) { 
      $product_id = $values['product_id'];
	
      if(has_term(  $category,  'product_cat', $product_id )){
        $ccatid=get_the_terms( $product_id, "product_cat" );
		$parentid=0;
		if(!empty($ccatid)){
			foreach($ccatid as $cctai){
				if($cctai->parent==0){
					$parentid=$cctai->term_id;
				}
			}
		}  
        $price=$values['line_subtotal'];
		if($parentid!=0){  
			if(isset($ccarray[$parentid])){
			  $ccarray[$parentid]=$ccarray[$parentid]+$price;
			}else{
			  $ccarray[$parentid]=$price;
			}
		}	
      } 
    }
  } 
  update_option("demo",$ccarray);
  if(!empty($ccarray)){
    foreach ($ccarray as $cc=>$value) {
      if($value<=$max_limit){
        $surcharge=$surcharge+$per_item_price;
      }
    }
  } 

  if($surcharge!=0.00){
    $woocommerce->cart->add_fee( $c_name, $surcharge, true, '' );
  }  
    
}
//print_r(get_option("demo"));
/*
function woocommerce_custom_surcharge() {
  global $woocommerce;

  if ( is_admin() && ! defined( 'DOING_AJAX' ) )
    return;

  $surcharge = 0.00;
    $items = $woocommerce->cart->get_cart();
    if(!empty($items)){
       foreach($items as $item => $values) { 
       $product_id = $values['product_id'];
       if(has_term(  'gear',  'product_cat', $product_id )){
         $price=$values['line_subtotal']; 
         if($price<=150){
           $surcharge=$surcharge+20;
         }
       }
       
     }
  } 
  $woocommerce->cart->add_fee( 'Extra Shipping Fee', $surcharge, true, '' );

}*/