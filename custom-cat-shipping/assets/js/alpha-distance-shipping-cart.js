jQuery(document).ready(function(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	 if(dd<10){
	        dd='0'+dd
	    } 
	    if(mm<10){
	        mm='0'+mm
	    } 

	today = yyyy+'-'+mm+'-'+dd;
	/*document.getElementById("alpha_date_pickup").setAttribute("min", today);*/
	var week_off_options= jQuery('#week_off_options').val();
	var day_off_options= jQuery('#day_off_options').val();
	if(week_off_options!=""){
		week_off_options=week_off_options.split(',');
	}
	if(day_off_options!=""){
		day_off_options=day_off_options.split('|');
	}
	
	jQuery(document).on('input','input[id="alpha_date_pickup"]',function(e){
		var days=[];
		  const day = (new Date(jQuery(this).val())).getDay();
		  
		  //console.log(jQuery(this).val());
		  if(week_off_options!=""){
			  if ( week_off_options.includes(day.toString())==true) {
			  	e.preventDefault();
			  	jQuery(this).val("");
			  	alert('Selected Day Not Available');
			    return false;
			  }
			}  
		  //console.log(day_off_options);
		  if(day_off_options!=""){
			  if ( day_off_options.includes(jQuery(this).val())==true) {
			  	e.preventDefault();
			  	jQuery(this).val("");
			  	alert('Selected Date Not Available');
			    return false;
			  }
		 }	  
		  return true;
	});


	//jQuery('.shipping-calculator-button').click();
    jQuery('.woocommerce-shipping-calculator button[name="calc_shipping"]').text("Enter Zip Code");
	jQuery(document).on('click','.zip-check-alpha',function(){
		alert('Enter your postal/zip code into the Above field To Enable Checkout');
	});

	jQuery(document).on('click','.datetime-ship-alpha',function(e){
		e.preventDefault();
		var date=jQuery('#alpha_date_pickup').val();
		var time=jQuery('#alpha_time_pickup option:selected').val();
		//var us_remark=jQuery('#us_remark').val();
		if(date!=''&&time!=''){
			var data={
		 		'action': 'alpha_session_shipping',
		 		'date':date,
		 		'time':time,
		 		
		 	};
		 	jQuery.post(ajaxurl, data, function(response) {
				jQuery('.checkout-button')[0].click();
			});
		}else{
			alert('Enter your Date/Time/Remarks  into the Above fields To Enable Checkout');
		}
		
	});

});