<?php
/**
 * WC_Shipping_Tyche class.
 *
 * @class 		WC_Shipping_alsd
 * @version		1.0.0
 * @package		Shipping-for-WooCommerce/Classes
 * @category	Class
 * @author 		Catrages
 */

if (!class_exists('Cat_Shipping_Method')) {
	class Cat_Shipping_Method extends WC_Shipping_Method {

		public function __construct() {
			$this->id                 = 'Cat_Shipping_Method'; // Id for your shipping method. Should be uunique.
			$this->method_title = __('Cat Distance Settings', 'Cat');  // Title shown in admin
			

			$this->enabled = isset($this->settings['enabled']) ? $this->settings['enabled'] : 'yes'; // This can be added as an setting but for this example its forced enabled
			$this->title = isset($this->settings['c_name']) ? $this->settings['c_name'] : __('Extra Shipping Fee', 'Cat'); // This can be added as an setting but for this example its forced.

			$this->category = isset($this->settings['category']) ? $this->settings['category'] : 'gear'; // This can be added as an setting but for this example its forced enabled
			$this->max_limit = isset($this->settings['max_limit']) ? $this->settings['max_limit'] : '150'; 
			$this->per_item_price  = isset($this->settings['per_item_price']) ? $this->settings['per_item_price'] : '20';

			


			$this->init();
		}

		/**
		 * Init your settings
		 *
		 * @access public
		 * @return void
		 */
		function init() {
			// Load the settings API
			$this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
			$this->init_settings(); // This is part of the settings API. Loads settings you previously init.

			// Save settings in admin if you have any defined
			add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
		}

		/**
		 * Init your Form settings
		 *
		 * @access public
		 * @return void
		 */

		function init_form_fields(){
			$args = array(
                'type'                     => 'product',
				'parent'                   => 0,
                'orderby'                  => 'name',
                'order'                    => 'ASC',
                'hide_empty'               => FALSE,
                'hierarchical'             => 1,
                'exclude'                  => '',
                'include'                  => '',
                'taxonomy'                 => 'product_cat',
                'pad_counts'               => false ,
              );
              
              $terms = get_terms( $args ); 
              $new_cat=[];
              if(!empty($terms)){
              	foreach ($terms as $cterm){
              		$new_cat[$cterm->term_id]=$cterm->name;
              	}
              }
			$this->form_fields = array(
				 'enabled' => array(
				 'title' => __('Category Shipping Enable', 'Cat'),
				 'type' => 'checkbox',
				 'default' => 'yes'
				 ),
				 'c_name' => array(
				 'title' => __('Title', 'Cat'),
				 'type' => 'text',
				 'default' => 'Extra Shipping Fee'
				 ),
				 'category' => array(
				 'title' => __('category', 'Cat'),
				 'type' => 'multiselect',
				 'options' => $new_cat
				 ),
				 'max_limit' => array(
				 'title' => __('Price max Then limit', 'Cat'),
				 'type' => 'text',
				 'default' => 150
				 ),
				'per_item_price' => array(
				 'title' => __('Price Per Item', 'Cat'),
				 'type' => 'text',
				 'default' => 20
				),

				'token_data_sec'=> array(
					'type'  => 'token_data',
					'title' => __( '', 'wcsdm' ),
				),
				

				

			);
		}


		/**
		 * Generate table_rates field.
		 *
		 * @since    1.0.0
		 * @param string $key Input field key.
		 * @param array  $data Settings field data.
		 */
		public function generate_token_data_html( $key, $data ) {
			
			
			ob_start();
			
			if(isset($this->settings['enabled'])){
				$enabled=$this->settings['enabled'];
				update_option('ccatship_enabled',$enabled);
			}
			if(isset($this->settings['c_name'])){
				$c_name=$this->settings['c_name'];
				update_option('ccatship_c_name',$c_name);
			}
			if(isset($this->settings['category'])){
				$category=$this->settings['category'];
				update_option('ccatship_category',$category);
			}
			if(isset($this->settings['max_limit'])){
				$max_limit=$this->settings['max_limit'];
				update_option('ccatship_max_limit',$max_limit);
			}
			if(isset($this->settings['per_item_price'])){
				$per_item_price=$this->settings['per_item_price'];
				update_option('ccatship_per_item_price',$per_item_price);
			}
			
			return ob_get_clean();

		}

	

	}
}


